require("dotenv/config");
const express = require("express");
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const port = process.env.PORT;
const swaggerUi = require("swagger-ui-express");
swaggerDocument = require("./swagger.json");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get("/", (req, res) => {
  res.status(200).send({ status: "/api-docs" });
});

const db = require("./app/models");
require("./app/routers/router")(app);

function createAdmin() {
  db.Role.create({
    roleId: 1,
    name: "ADMIN",
  });

  db.Role.create({
    roleId: 2,
    name: "DOKTER",
  });

  db.Role.create({
    roleId: 3,
    name: "KASIR",
  });

  db.User.create({
    username: "admin0001",
    password: bcrypt.hashSync("admin", 8),
    nik: 1,
  })
    .then((users) => {
      db.Role.findAll({
        where: {
          name: "ADMIN",
        },
      })
        .then((roles) => {
          users.setRoles(roles);
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

db.sequelize
  .sync({
    // force: true,
  })
  .then(() => {
    // createAdmin();
    app.listen(port, () => {
      console.log(`Server running on port ${port}`);
    });
  })
  .catch((err) => {
    console.log(err);
  });
