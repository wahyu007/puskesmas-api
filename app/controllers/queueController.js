const db = require("../models/index");
const Queue = db.Queue;

exports.create = (req, res) => {
  const queue = {
    queueId: req.queueId,
    patientId: req.body.patientId,
  };

  Queue.create(queue)
    .then((data) => {
      res.status(201).send({
        auth: true,
        status: "success",
        message: "none",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "none",
        response: err.message || "Some error ocured while creating the queue.",
      });
    });
};

exports.findAll = (req, res) => {
  Queue.findAll({ include: "patient" })
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on queues",
        response: err.message || "Some error while retriving queues.",
      });
    });
};

exports.delete = (req, res) => {
  const queueId = req.params.queueId;

  Queue.destroy({
    where: { queueId: queueId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(203).send({
          auth: true,
          status: "success",
          message: "Queue was deleted successfully!",
          response: num,
        });
      } else {
        res.status(402).send({
          auth: false,
          status: "error",
          message: `Cannot delete user with queueId = ${queueId}`,
          response: err.message || "Some error while retriving queues.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Could not Delete Queue with queueId =" + queueId,
        response: err.message || "Some error while retriving queues.",
      });
    });
};
