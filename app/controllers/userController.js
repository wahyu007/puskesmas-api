const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require("../models/index");
const User = db.User;

exports.create = (req, res) => {
  if (!req.body.name) {
    res.status(401).send({
      auth: false,
      status: "error",
      message: "Content cannot empty",
      response: "none",
    });

    return;
  }

  const user = {
    username: req.body.name,
    password: bcrypt.hashSync(req.body.password, 8),
    nik: req.body.nik,
  };

  User.create(user)
    .then((data) => {
      res.status(201).send({
        auth: false,
        status: "success",
        message: "none",
        response: data,
      });
    })
    .catch((err) => {
      res.status(401).send({
        auth: false,
        status: "error",
        message: "none",
        response: err.message,
      });
    });
};

exports.login = (req, res) => {
  const username = req.body.username;

  User.findOne({ where: { username: username } })
    .then((data) => {
      console.log(data);
      var passValid = bcrypt.compareSync(req.body.password, data.password);
      if (passValid) {
        var token =
          "KEY " +
          jwt.sign(
            {
              nik: data.nik,
            },
            "secretAuth",
            {
              expiresIn: 86400,
            }
          );

        res.status(200).send({
          auth: true,
          status: "success",
          message: "Login successfully",
          response: token,
          data: data,
        });
      } else {
        res.status(403).send({
          auth: false,
          status: "error",
          message: "Password wrong!",
          response: null,
        });
      }
    })
    .catch((err) => {
      res.status(404).send({
        auth: false,
        status: "error",
        message: "Username ${username} not registered",
        response: err.message,
      });
    });
};
