const db = require("../models/index");
const Medicine = db.Medicine;

exports.create = (req, res) => {
  const medicine = {
    medicineId: req.medicineId,
    name: req.body.name,
  };

  Medicine.create(medicine)
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        message: "none",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "none",
        response:
          err.message || "Some error ocured while creating the medicine.",
      });
    });
};

exports.findAll = (req, res) => {
  Medicine.findAll()
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on medicines",
        response: err.message || "Some error while retriving medicines.",
      });
    });
};

exports.findOne = (req, res) => {
  medicineId = req.params.medicineId;
  Medicine.findByPk(medicineId)
    .then((user) => {
      if (user != null) {
        res.status(200).send({
          auth: true,
          status: "success",
          message: "none",
          response: user,
        });
      } else {
        res.status(401).send({
          auth: false,
          status: "error",
          message: "Medicine Not Found",
          response: null,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Medicine Not Found",
        response: err.message | "error",
      });
    });
};

exports.update = (req, res) => {
  const medicineId = req.params.medicineId;

  Medicine.update(req.body, {
    where: { medicineId: medicineId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(200).send({
          auth: true,
          status: "success",
          message: "Medicine was Updated",
          response: num,
        });
      } else {
        res.status(402).send({
          auth: false,
          status: "error",
          message: `Cannot update user with medicineId = ${medicineId}`,
          response: err.message || "Some error while retriving medicines.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Error updating user with medicineId " + medicineId,
        response: err.message || "Some error while retriving medicines.",
      });
    });
};

exports.delete = (req, res) => {
  const medicineId = req.params.medicineId;

  Medicine.destroy({
    where: { medicineId: medicineId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(203).send({
          auth: true,
          status: "success",
          message: "Medicine was deleted successfully!",
          response: num,
        });
      } else {
        res.status(402).send({
          auth: false,
          status: "error",
          message: `Cannot delete user with medicineId = ${medicineId}`,
          response: err.message || "Some error while retriving medicines.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Could not Delete Medicine with medicineId =" + medicineId,
        response: err.message || "Some error while retriving medicines.",
      });
    });
};
