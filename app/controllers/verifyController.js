const jwt = require("jsonwebtoken");
const User = require("./../models").User;

module.exports = {
  verifyToken(req, res, next) {
    let tokenHandler = req.headers["x-access-token"];

    if (!tokenHandler || tokenHandler.split(" ")[0] !== "KEY") {
      return res.status(401).send({
        auth: false,
        status: "error",
        message: "Incorect token format",
        response: null,
      });
    }

    let token = tokenHandler.split(" ")[1];
    if (!token) {
      return res.status(401).send({
        auth: false,
        status: "error",
        message: "No token provided",
        response: null,
      });
    }

    jwt.verify(token, "secretAuth", (err, decode) => {
      if (err) {
        return res.status(401).send({
          auth: false,
          status: "Error",
          message: err.message,
          response: null,
        });
      }

      req.nik = decode.nik;
      next();
    });
  },
  isAdmin(req, res, next) {
    User.findByPk(req.nik).then((user) => {
      // console.log(user);
      user.getRoles().then((roles) => {
        console.log(roles[0].name);
        if (roles[0].name == "ADMIN") {
          next();
          return;
        }
        res
          .status(401)
          .send({
            status: "error",
            auth: false,
            message: "Require Admin Role",
          })
          .catch((err) => {
            res.status(500).send({
              status: "error",
              message: err,
            });
          });

        return;
      });
    });
  },
};
