const db = require("../models/index");
const MedicalRecord = db.MedicalRecord;
const Patient = db.Patient;

exports.create = (req, res) => {
  const medicalRecord = {
    // medicalRecordId: req.medicalRecordId,
    physical: req.body.physical,
    complaint: req.body.complaint,
    diagnosis: req.body.diagnosis,
    therapy: req.body.therapy,
    patientId: req.body.patientId,
  };

  MedicalRecord.create(medicalRecord)
    .then((data) => {
      res.status(201).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "none",
        response:
          err.message || "Some error ocured while creating the medicalRecord.",
      });
    });
};

exports.findAll = (req, res) => {
  MedicalRecord.findAll()
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on medicalRecords",
        response: err.message || "Some error while retriving medicalRecords.",
      });
    });
};

exports.findOne = (req, res) => {
  medicalRecordId = req.params.medicalRecordId;
  MedicalRecord.findByPk(medicalRecordId)
    .then((user) => {
      if (user != null) {
        res.status(200).send({
          auth: true,
          status: "success",
          message: "none",
          response: user,
        });
      } else {
        res.status(402).send({
          auth: false,
          status: "error",
          message: "MedicalRecord Not Found",
          response: null,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "MedicalRecord Not Found",
        response: err.message | "error",
      });
    });
};

exports.patientHistory = (req, res) => {
  patientId = req.params.patientId;
  Patient.findAll({
    where: {
      patientId: patientId,
    },
    include: "history",
  })
    .then((data) => {
      res.status(200).send({
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "MedicalRecord Not Found",
        response: err | "error",
      });
    });
};

exports.update = (req, res) => {
  const medicalRecordId = req.params.medicalRecordId;

  MedicalRecord.update(req.body, {
    where: { medicalRecordId: medicalRecordId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(201).send({
          auth: true,
          status: "success",
          message: "MedicalRecord was Updated",
          response: num,
        });
      } else {
        res.send({
          auth: false,
          status: "error",
          message: `Cannot update user with medicalRecordId = ${medicalRecordId}`,
          response: err.message || "Some error while retriving medicalRecords.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Error updating user with medicalRecordId " + medicalRecordId,
        response: err.message || "Some error while retriving medicalRecords.",
      });
    });
};

exports.delete = (req, res) => {
  const medicalRecordId = req.params.medicalRecordId;

  MedicalRecord.destroy({
    where: { medicalRecordId: medicalRecordId },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          auth: true,
          status: "success",
          message: "MedicalRecord was deleted successfully!",
          response: num,
        });
      } else {
        res.send({
          auth: false,
          status: "error",
          message: `Cannot delete user with medicalRecordId = ${medicalRecordId}`,
          response: err.message || "Some error while retriving medicalRecords.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message:
          "Could not Delete MedicalRecord with medicalRecordId =" +
          medicalRecordId,
        response: err.message || "Some error while retriving medicalRecords.",
      });
    });
};
