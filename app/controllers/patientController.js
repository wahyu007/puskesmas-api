const db = require("../models/index");
const Patient = db.Patient;
const { Op } = require("sequelize");

exports.create = (req, res) => {
  const patient = {
    patientId: req.patientId,
    name: req.body.name,
    address: req.body.address,
    age: req.body.age,
  };

  Patient.create(patient)
    .then((data) => {
      res.status(201).send({
        auth: true,
        status: "success",
        message: "none",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "none",
        response:
          err.message || "Some error ocured while creating the patient.",
      });
    });
};

exports.findAll = (req, res) => {
  Patient.findAll()
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on patients",
        response: err.message || "Some error while retriving patients.",
      });
    });
};

exports.searchPatient = (req, res) => {
  patientName = req.params.patientName;
  Patient.findAll({
    where: {
      name: {
        [Op.iLike]: `%${patientName}%`,
      },
    },
  })
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on patients",
        response: err.message || "Some error while retriving patients.",
      });
    });
};

exports.findOne = (req, res) => {
  patientId = req.params.patientId;
  Patient.findByPk(patientId)
    .then((user) => {
      if (user != null) {
        res.status(200).send({
          auth: true,
          status: "success",
          message: "none",
          response: user,
        });
      } else {
        res.status(404).send({
          auth: false,
          status: "error",
          message: "Patient Not Found",
          response: null,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Patient Not Found",
        response: err.message | "error",
      });
    });
};

exports.update = (req, res) => {
  const patientId = req.params.patientId;

  Patient.update(req.body, {
    where: { patientId: patientId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(204).send({
          auth: true,
          status: "success",
          message: "Patient was Updated",
          response: num,
        });
      } else {
        res.status(404).send({
          auth: false,
          status: "error",
          message: `Cannot update user with patientId = ${patientId}`,
          response: err.message || "Some error while retriving patients.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Error updating user with patientId " + patientId,
        response: err.message || "Some error while retriving patients.",
      });
    });
};

exports.delete = (req, res) => {
  const patientId = req.params.patientId;

  Patient.destroy({
    where: { patientId: patientId },
  })
    .then((num) => {
      if (num == 1) {
        res.status(204).send({
          auth: true,
          status: "success",
          message: "Patient was deleted successfully!",
          response: num,
        });
      } else {
        res.status(404).send({
          auth: false,
          status: "error",
          message: `Cannot delete user with patientId = ${patientId}`,
          response: err.message || "Some error while retriving patients.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Could not Delete Patient with patientId =" + patientId,
        response: err.message || "Some error while retriving patients.",
      });
    });
};
