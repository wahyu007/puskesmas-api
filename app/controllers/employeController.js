const db = require("../models/index");
const Employe = db.Employe;

exports.create = (req, res) => {
  const employe = {
    // nik: req.body.nik,
    name: req.body.name,
    position: req.body.position,
  };

  Employe.create(employe)
    .then((data) => {
      res.status(201).send({
        status: "success",
        message: "none",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        status: "error",
        message: err.errors || "Some error ocured while creating the employe.",
      });
    });
};

exports.findAll = (req, res) => {
  Employe.findAll()
    .then((data) => {
      res.status(200).send({
        auth: true,
        status: "success",
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "error on employes",
        response: err.message || "Some error while retriving employes.",
      });
    });
};

exports.findOne = (req, res) => {
  nik = req.params.nik;
  Employe.findByPk(nik)
    .then((user) => {
      if (user != null) {
        res.status(200).send({
          auth: true,
          status: "success",
          message: "none",
          response: user,
        });
      } else {
        res.status(401).send({
          auth: false,
          status: "error",
          message: "Employe Not Found",
          response: null,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Employe Not Found",
        response: err.message | "error",
      });
    });
};

exports.update = (req, res) => {
  const nik = req.params.nik;

  Employe.update(req.body, {
    where: { nik: nik },
  })
    .then((num) => {
      if (num == 1) {
        res.status(203).send({
          auth: true,
          status: "success",
          message: "Employe was Updated",
          response: num,
        });
      } else {
        res.status(401).send({
          auth: false,
          status: "error",
          message: `Cannot update user with nik = ${nik}`,
          response: err.message || "Some error while retriving employes.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Error updating user with nik " + nik,
        response: err.message || "Some error while retriving employes.",
      });
    });
};

exports.delete = (req, res) => {
  const nik = req.params.nik;

  Employe.destroy({
    where: { nik: nik },
  })
    .then((num) => {
      if (num == 1) {
        res.status(203).send({
          auth: true,
          status: "success",
          message: "Employe was deleted successfully!",
          response: num,
        });
      } else {
        res.status(401).send({
          auth: false,
          status: "error",
          message: `Cannot delete user with nik = ${nik}`,
          response: err.message || "Some error while retriving employes.",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        auth: false,
        status: "error",
        message: "Could not Delete Employe with nik =" + nik,
        response: err.message || "Some error while retriving employes.",
      });
    });
};
