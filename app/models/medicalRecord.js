'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class MedicalRecord extends Model {
        static associate(models){
            // MedicalRecord.belongsTo(models.Patient, {
            //     foreignKey: 'patientId',
            //     as : 'patient'
            // })
        }
    };
    MedicalRecord.init({
        medicalRecordId : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        patientId:{
            type: DataTypes.INTEGER
        },
        physical:{
            type: DataTypes.STRING
        },
        complaint: {
            type: DataTypes.STRING
        },
        diagnosis: {
            type: DataTypes.STRING
        },
        therapy: {
            type: DataTypes.STRING
        },
    },{
        sequelize,
        modelName: 'MedicalRecord',
    });
    return MedicalRecord;
}