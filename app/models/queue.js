'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Queue extends Model {
        static associate(models){
            Queue.belongsTo(models.Patient, {
                foreignKey: 'patientId', 
                as: 'patient'
            })
        }
    };
    Queue.init({
        queueId : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        patientId : {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: true
        },
    },{
        sequelize,
        modelName: 'Queue',
    });
    return Queue;
}