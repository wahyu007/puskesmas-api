'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Medicine extends Model {
        static associate(models){

        }
    };
    Medicine.init({
        medicineId : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        }
    },{
        sequelize,
        modelName: 'Medicine',
    });
    return Medicine;
}