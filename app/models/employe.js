'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Employe extends Model {
        static associate(models){

        }
    };
    Employe.init({
        nik : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        position: {
            type: DataTypes.STRING
        }
    },{
        sequelize,
        modelName: 'Employe',
    });
    return Employe;
}