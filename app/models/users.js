'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate(models){
            User.belongsToMany(models.Role, {
                through: 'userRoles',
                foreignKey: {
                    name : 'userId',
                    type: DataTypes.INTEGER
                },
                otherKey: 'roleId'
              })
        }
    };
    User.init({
        userId : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        nik: {
            type: DataTypes.INTEGER
        }
    },{
        sequelize,
        modelName: 'User',
    });
    return User;
}