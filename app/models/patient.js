'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Patient extends Model {
        static associate(models){
            Patient.hasMany(models.MedicalRecord, {
                foreignKey: 'patientId',
                as : 'history'
            })
        }
    };
    Patient.init({
        patientId : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        age: {
            type: DataTypes.INTEGER
        }
    },{
        sequelize,
        modelName: 'Patient',
    });
    return Patient;
}