module.exports = (app) => {
  const userController = require("../controllers/userController");
  const employeController = require("../controllers/employeController");
  const medicineController = require("../controllers/medicineController");
  const patientController = require("../controllers/patientController");
  const mRecordController = require("../controllers/mRecController");
  const verifyController = require("../controllers/verifyController");
  const queueController = require("../controllers/queueController");

  let router = require("express").Router();

  router.post("/login", userController.login);
  router.post("/user", userController.create);

  router.get(
    "/employe/all",
    [verifyController.verifyToken, verifyController.isAdmin],
    employeController.findAll
  );
  router.get(
    "/employe/:nik",
    [verifyController.verifyToken, verifyController.isAdmin],
    employeController.findOne
  );
  router.post(
    "/employe",
    [verifyController.verifyToken, verifyController.isAdmin],
    employeController.create
  );
  router.put(
    "/employe/update/:nik",
    [verifyController.verifyToken, verifyController.isAdmin],
    employeController.update
  );
  router.delete(
    "/employe/delete/:nik",
    [verifyController.verifyToken, verifyController.isAdmin],
    employeController.delete
  );

  router.get(
    "/patient/all",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.findAll
  );
  router.get(
    "/patient/:patientId",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.findOne
  );
  router.get(
    "/patient/search/:patientName",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.searchPatient
  );
  router.post(
    "/patient",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.create
  );
  router.put(
    "/patient/update/:patientId",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.update
  );
  router.delete(
    "/patient/delete/:patientId",
    [verifyController.verifyToken, verifyController.isAdmin],
    patientController.delete
  );

  router.get(
    "/medicine/all",
    [verifyController.verifyToken, verifyController.isAdmin],
    medicineController.findAll
  );
  router.get(
    "/medicine/:medicineId",
    [verifyController.verifyToken, verifyController.isAdmin],
    medicineController.findOne
  );
  router.post(
    "/medicine",
    [verifyController.verifyToken, verifyController.isAdmin],
    medicineController.create
  );
  router.put(
    "/medicine/update/:medicineId",
    [verifyController.verifyToken, verifyController.isAdmin],
    medicineController.update
  );
  router.delete(
    "/medicine/delete/:medicineId",
    [verifyController.verifyToken, verifyController.isAdmin],
    medicineController.delete
  );

  router.get("/mRecord/all", mRecordController.findAll);
  router.get("/mRecord/:medicalRecordId", mRecordController.findOne);
  router.get("/mRecord/patient/:patientId", mRecordController.patientHistory);
  router.post("/mRecord", mRecordController.create);
  router.put("/mRecord/update/:medicalRecordId", mRecordController.update);
  router.delete("/mRecord/delete/:medicalRecordId", mRecordController.delete);

  router.get(
    "/queue/all",
    [verifyController.verifyToken, verifyController.isAdmin],
    queueController.findAll
  );
  router.post(
    "/queue",
    [verifyController.verifyToken, verifyController.isAdmin],
    queueController.create
  );
  router.delete(
    "/queue/:queueId",
    [verifyController.verifyToken, verifyController.isAdmin],
    queueController.delete
  );

  app.use("/api/v1/puskesmas", router);
};
